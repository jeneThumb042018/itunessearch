 # README #
 
 This README would normally document whatever steps are necessary to get your application up and running.
 
 ### What is this repository for? ###
 
 * A master-detail application that contains at least one dependency. This application should display a list of items obtained from a iTunes Search API and show a detailed view for each item.
 * See iTunes Web Service Documentation: https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/#searching
 * The app demonstrates the use of CoreData for structured storage and Userdefaults for basic status storage.
 
 ### How do I get set up? ###
 
 * Simply download the project. Open the .xcode file. Run.
