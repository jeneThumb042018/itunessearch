//
//  IconCell.swift
//
//  Created by Jene de Borja on 01/08/2019.




import Foundation
import UIKit

class IconCell: UITableViewCell {
    static let id = "IconCell"
    
    @IBOutlet var topLabel : UILabel!
    @IBOutlet var botLabel : UILabel!
    @IBOutlet var thumbnail : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.topLabel.backgroundColor = .clear
        self.botLabel.backgroundColor = .clear
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.layoutIfNeeded()
        self.topLabel.text = ""
        self.botLabel.text = ""
        self.layoutIfNeeded()
    }
    
    
    public func configCell(track:Tracks)
    {
        self.topLabel.text = track.name
        self.botLabel.text = track.artist
        
        CoreRoot.shared.downloadImage(ImageView:self.thumbnail, track:track)
    }

    
}

