//
//  SS_OverlayLoading.swift
//
//  Created by Jene de Borja on 01/08/2019.


import Foundation
import UIKit

typealias CoreRoot = SS_OverlayLoading
class SS_OverlayLoading: UISplitViewController, UISplitViewControllerDelegate
{
    static let shared = UIApplication.shared.delegate?.window!?.rootViewController as! SS_OverlayLoading
    let LoadingOverlay : UIView = UIView()
    let spinner : UIActivityIndicatorView = UIActivityIndicatorView.init(style:  UIActivityIndicatorView.Style.whiteLarge)
    var isLoading : Bool = false

    override func viewDidLoad() {
        
        
        UIApplication.shared.isIdleTimerDisabled = true
        super.viewDidLoad()

        initialiazeLoading()
    }
    
    func initialiazeLoading()
    {
        
        LoadingOverlay.translatesAutoresizingMaskIntoConstraints = false
        spinner.translatesAutoresizingMaskIntoConstraints = false
        
        setupBlur()
        LoadingOverlay.addSubview(spinner)
        LoadingOverlay.alpha = 0
        LoadingOverlay.isHidden = true
        spinner.color = .green
        self.view.addSubview(self.LoadingOverlay)
        let width = NSLayoutConstraint(item: LoadingOverlay, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1, constant: 0)
        let height = NSLayoutConstraint(item: LoadingOverlay, attribute: .height, relatedBy: .equal, toItem: view, attribute: .height, multiplier: 1, constant: 0)
        view.addConstraints([height, width])
        
        
        NSLayoutConstraint.activate([
            spinner.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo:  self.view.centerYAnchor)
            ])
    }
    
    func setupBlur()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = LoadingOverlay.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        LoadingOverlay.addSubview(blurEffectView)
    }
    
    
    // MARK: - PUBLIC HANDLERS
    // LOADING HANDLERS
    public func showLoading(completion:@escaping ()->Void) {
        if !CoreRoot.shared.isLoading
        {
            
            CoreRoot.shared.isLoading = true
            
            LoadingOverlay.alpha = 0.0
            LoadingOverlay.isHidden = false
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseInOut, animations: {() -> Void in
                
                self.LoadingOverlay.alpha = 1.0
            }, completion: {(finished: Bool) -> Void in
                
                self.spinner.startAnimating()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {  completion()  }
                
            })
        }
        else
        {  completion() }
        
    }
    
    public func showLoading()
    {
        self.spinner.startAnimating()
        LoadingOverlay.alpha = 1.0
        LoadingOverlay.isHidden = false
        CoreRoot.shared.isLoading = true
    }
  
    public func hideLoading()
    {
        self.spinner.stopAnimating()
        LoadingOverlay.alpha = 0.0
        LoadingOverlay.isHidden = true
        CoreRoot.shared.isLoading = false
    }

    // MARK: - PUBLIC HANDLERS
    // ALERTVIEW
    func showAlertView (_ title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }


}




