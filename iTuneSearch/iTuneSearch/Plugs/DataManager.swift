//
//  DataManager.swift
//
//  Created by Jene de Borja on 01/08/2019.


import Foundation
import UIKit
import CoreData

class DataManager {
    
    struct entity {
        public static let trackList = "Tracks"
        
    }
    
    public static let shared: DataManager = DataManager()
    public var managedObjectContext: NSManagedObjectContext!
    private var applicationDelegate: AppDelegate!
    
    public init() {
        self.applicationDelegate = UIApplication.shared.delegate as? AppDelegate
        self.managedObjectContext = self.applicationDelegate.managedObjectContext
    }
    
    public func createEntity<T>(_ entityName: String) -> T? {
        guard let entity = NSEntityDescription.entity(forEntityName: entityName, in: self.managedObjectContext) else { return nil }
        guard let managedObject = NSManagedObject(entity: entity, insertInto: self.managedObjectContext) as? T else { return nil }
        return managedObject
    }
    
    public func getEntity<T>(entityName: String, predicate: NSPredicate?) -> T? {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetch.returnsObjectsAsFaults = false
        fetch.includesPendingChanges = false
        
        if let predicate = predicate {
            fetch.predicate = predicate
        }
        
        do {
            guard let fetchResults = try self.managedObjectContext.fetch(fetch) as? [NSManagedObject] else { return nil }
            guard let manageObject = fetchResults.first else { return nil }
            guard let entity = manageObject as? T else { return nil }
            return entity
        } catch {
            CoreRoot.shared.Log(content: "Error with get request: \(error)")
            return nil
        }
    }
    
    public func getGroupEntity<T>(entityName: String, predicate: NSPredicate?) -> [T]? {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetch.returnsObjectsAsFaults = false
        
        if let predicate = predicate {
            fetch.predicate = predicate
        }
        
        do {
            guard let fetchResults = try self.managedObjectContext.fetch(fetch) as? [NSManagedObject] else { return nil }
            return fetchResults.map { return $0 as! T }
        } catch {
            CoreRoot.shared.Log(content:"Error with request: \(error)")
            return nil
        }
    }
    
    public func saveEntity(_ entity: String) {
        do {
            try self.managedObjectContext.save()
            CoreRoot.shared.Log(content:"saved \(entity)")
        } catch {
            CoreRoot.shared.Log(content:"Error with save request: \(error)")
        }
    }
    
    public func deleteEntity(_ entity: NSManagedObject) {
        do {
            self.managedObjectContext.delete(entity)
            try self.managedObjectContext.save()
            CoreRoot.shared.Log(content:"\(String(describing: entity.entity.managedObjectClassName)) Deleted!")
            
        } catch let error as NSError  {
            CoreRoot.shared.Log(content:"delete \(error), \(error.userInfo)")
        }
    }
    

    
}
