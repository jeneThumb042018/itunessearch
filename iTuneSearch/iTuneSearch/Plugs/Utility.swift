//
//  Utility.swift
//
//  Created by Jene de Borja on 01/08/2019.


struct websvc {
    static let base = "https://itunes.apple.com/search?"
    static let token = ""
    struct keys {
        static let lastupdate   = "ksuccessful_fetch"
        static let lastkey      = "klastSearchKey"
        static let lastview     = "klastSaveView"
    }
}

enum HttpMethod : String
{
    case kGET = "GET"
    case kPOST = "POST"
    case kPUT = "PUT"
    case kDE = "DELETE"
    var SELECT: String {
        return self.rawValue
    }
}

typealias DataResponse = Any
typealias hasResponse = Bool
typealias ErrorCode = Int



import Foundation
import UIKit
extension SS_OverlayLoading
{
    //Date Formatter
    class func currentStringDate()->String
    {
        let dateformatter = DateFormatter.init()
        dateformatter.dateFormat = "dd/MM/yy [HH:mm]"
        dateformatter.locale = NSLocale.current
        
        return dateformatter.string(from:Date())
        
    }
    
    //NSUserdefaults handler
    class func StoreLastSave(data:String, byKey:String)
    {
        let user = UserDefaults.standard
        user.setValue(data, forKey: byKey)
        user.synchronize()
    }
    
    class func LoadLastSave(Key:String)->String? {
        let user = UserDefaults.standard
        if let string = user.value(forKey: Key) as? String, (string.count > 0) { return string }
       
        return nil
    }
    
    
    class func clearLastData(Key:String)
    {
        let user = UserDefaults.standard
        user.removeObject(forKey: Key)
        user.synchronize()
    }
 

}


