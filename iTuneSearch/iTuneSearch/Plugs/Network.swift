//
//  Network.swift
//
//  Created by Jene de Borja on 01/08/2019.


import Foundation
import UIKit



extension SS_OverlayLoading
{
    public func Log(content:Any)
    {
        #if DEBUG
        print(content)
        #elseif RELEASE
        #endif
    }
    
    func RegisterRequest(baseURL:String = websvc.base, tokenRequired:Bool = false, dataTransfered: Any? = [], theService extSvc: String = "", Method:HttpMethod = .kGET) -> NSMutableURLRequest {
        
        let varURLStr: String = baseURL
     
        
        let urlString: String = varURLStr + extSvc
        CoreRoot.shared.Log(content: "websvc---> \(urlString)")
        CoreRoot.shared.Log(content: "content: \(dataTransfered ?? "none")")
        
        let escapedUrl: String = urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlFragmentAllowed)!
        
        
        
        let request: NSMutableURLRequest = NSMutableURLRequest()
        if dataTransfered != nil {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: dataTransfered!, options:[])
            } catch { CoreRoot.shared.Log(content: "ERROR RegisterCheck") }
        }
        
        
        request.url = NSURL(string: escapedUrl)! as URL
        request.httpMethod = Method.SELECT
        if tokenRequired == true {
            request.setValue(websvc.token, forHTTPHeaderField:"Authorization")
        }
        
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        
        
        return request
    }
    
    
    func LoadRequest(theRequest:NSMutableURLRequest, isJSON : Bool = false, loading:Bool = true, with returnCompletion:@escaping (DataResponse, hasResponse, ErrorCode)->Void) {
       
        
        if loading { CoreRoot.shared.showLoading() }
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task =  session.dataTask(with: theRequest as URLRequest, completionHandler: {(data, response, error) in
            var statC : Int = 0
            if let httpResponse = response as? HTTPURLResponse {
                statC = httpResponse.statusCode
                CoreRoot.shared.Log(content: "statusCode \(httpResponse.statusCode)")
            }
            var dataResult : Any = []
            var strAvail : Bool = false
            if ((data != nil) && (data!.count > 0)) {
                strAvail = true
                if isJSON
                {
                    do { dataResult = try JSONSerialization.jsonObject(with: data!, options:[]) }
                    catch let dataError {
                        // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                        
                        CoreRoot.shared.Log(content: dataError)
                        let jsonStr = String(data:data!, encoding: String.Encoding.utf8)
                        CoreRoot.shared.Log(content: "content of Data \(jsonStr!)")
                        
                    }
                    
                    
                }
                else  { dataResult = data! }
                
            }
            
            
            DispatchQueue.main.async {
                if loading { CoreRoot.shared.hideLoading() }
                returnCompletion(dataResult,strAvail, statC)
                
            }
            
            
        })
        
        task.resume( )
    }
    
    
    public func downloadImage(ImageView:UIImageView, track:Tracks)
    {
        if let iconFile = track.icon_file
        { ImageView.image = UIImage(data: iconFile as Data) }
        else
        {
            if let fileURL = track.icon_url
            {
                let request = CoreRoot.shared.RegisterRequest(baseURL:fileURL)
                CoreRoot.shared.LoadRequest(theRequest: request, isJSON: false, loading: false) { (imageFile, hasResp, ErrorCode) in
                    if let imagedata = imageFile as? NSData, let dLthumb = UIImage(data: imagedata as Data)
                    {
                        ImageView.image = dLthumb
                        
                        track.icon_file = imagedata
                        track.save()
                    }
                }
            }
        }
    }
    
 
}
