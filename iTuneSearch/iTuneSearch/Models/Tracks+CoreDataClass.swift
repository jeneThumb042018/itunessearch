//
//  Tracks+CoreDataClass.swift
//  iTuneSearch
//
//  Created by Jene de Borja on 01/08/2019.
//
//

import Foundation
import CoreData


public class Tracks: NSManagedObject {
    public class func updateEntity(id:String, attributes: [String:Any]) -> Tracks?
    {
        var track = Tracks.getTrack(id: id)
        if track == nil {
            track = DataManager.shared.createEntity(DataManager.entity.trackList)
            track?.track_id = id
        }
        
        track?.name = attributes["trackName"] as? String
        track?.artist = attributes["artistName"] as? String
        track?.price = attributes["trackPrice"] as? String
        track?.currency = attributes["currency"] as? String
        track?.icon_url = attributes["artworkUrl100"] as? String
        track?.genre = attributes["primaryGenreName"] as? String
        track?.artist_url = attributes["artistViewUrl"] as? String
        track?.track_url = attributes["trackViewUrl"] as? String
        track?.desc = attributes["longDescription"] as? String
        
        return track
    }
    
    public class func newEntity() -> Tracks {
        let track: Tracks! = DataManager.shared.createEntity(DataManager.entity.trackList)
        return track
    }
    
    public class func getTrack( id: String) -> Tracks? {
        let predicate = NSPredicate(format: "track_id = %@", id)
        guard let track: Tracks = DataManager.shared.getEntity(entityName: DataManager.entity.trackList, predicate: predicate) else { return nil }
        return track
    }
    
    public class func getAllTracks() -> [Tracks] {
        guard let tracklist: [Tracks] = DataManager.shared.getGroupEntity(entityName: DataManager.entity.trackList, predicate: nil) else { return [] }
        return tracklist
    }
    
    public class func saveAll(DataArray : [NSDictionary])
    {
        Tracks.deleteAll()
        for contents in DataArray
        {
            guard let trackID = contents["trackId"], let track = Tracks.updateEntity(id: "\(trackID)", attributes: contents as! [String : Any]) else { continue }
            track.save()
        }
    }
    
    public func save() {
        DataManager.shared.saveEntity(DataManager.entity.trackList)
    }
    
    
    public class func deleteAll() {
        let entitys = Tracks.getAllTracks()
        for entity in entitys { DataManager.shared.deleteEntity(entity) }
    }
}
