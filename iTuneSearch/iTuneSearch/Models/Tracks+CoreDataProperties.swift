//
//  Tracks+CoreDataProperties.swift
//  iTuneSearch
//
//  Created by Jene de Borja on 01/08/2019.
//
//

import Foundation
import CoreData


extension Tracks {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tracks> {
        return NSFetchRequest<Tracks>(entityName: "Tracks")
    }
    
    @NSManaged public var artist: String?
    @NSManaged public var artist_url: String?
    @NSManaged public var currency: String?
    @NSManaged public var desc: String?
    @NSManaged public var genre: String?
    @NSManaged public var icon_file: NSData?
    @NSManaged public var icon_url: String?
    @NSManaged public var name: String?
    @NSManaged public var price: String?
    @NSManaged public var track_id: String?
    @NSManaged public var track_url: String?
    
}
