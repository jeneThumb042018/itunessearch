//
//  DetailViewController.swift
//  iTuneSearch
//
//  Created by Jene de Borja on 01/08/2019.


import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var detailTextView :UITextView!
    @IBOutlet weak var detailImage :UIImageView!
    
    
    func configureView() {
        // Update the user interface for the detail item.
        
        if let detail = detailItem {
            if let label = detailTextView, let descStr = detail.desc {
                label.text = descStr
            }
            
            self.title = detail.name
            
            if let imageDisplay = detailImage {
                CoreRoot.shared.downloadImage(ImageView:imageDisplay, track:detail)
                
            }
            
            CoreRoot.StoreLastSave(data: detail.track_id!, byKey: websvc.keys.lastview)
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        configureView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if detailItem != nil {
            CoreRoot.clearLastData(Key: websvc.keys.lastview)
        }
    }
    
    var detailItem: Tracks? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    
}

