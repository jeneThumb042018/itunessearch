//
//  MasterViewController.swift
//  iTuneSearch
//
//  Created by Jene de Borja on 01/08/2019.


import UIKit
import CoreData

class MasterViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    var detailViewController: DetailViewController? = nil
    var managedObjectContext: NSManagedObjectContext? = nil
    private let TableHeight = 30.0
    private let appendKey = "&limit=25&amp;country=au&amp;media=music&amp;all"
    var SearchKeyString = ""
    
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(SearchAction(_:)))
        navigationItem.leftBarButtonItem = addButton
        
        let infoButton = UIButton(type: .infoLight)
        
        infoButton.addTarget(self, action: #selector(InfoAction(_:)), for: .touchUpInside)
        let infoBarButtonItem = UIBarButtonItem(customView: infoButton)
        navigationItem.rightBarButtonItem = infoBarButtonItem
        
        
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        let cellNib = UINib(nibName:IconCell.id, bundle:Bundle.main)
        self.tableView?.register(cellNib, forCellReuseIdentifier: IconCell.id)
        
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(refreshAction(_:)), for: .valueChanged)
        tableView.addSubview(refresher)
        
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = 100
        tableView.estimatedSectionHeaderHeight = 100
        
        let getallTracks = Tracks.getAllTracks()
        
        if let lastKeySearch = CoreRoot.LoadLastSave(Key: websvc.keys.lastkey)
        { self.SearchKeyString = lastKeySearch }
        
        if let lastUpdateString = CoreRoot.LoadLastSave(Key: websvc.keys.lastupdate), (getallTracks.count != 0)
        { self.configHeaderText(text: "LAST UPDATE: \(lastUpdateString)") }
        else { self.configHeaderText(text: "Result : Empty Record", alignment: .right) }
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let lastView_id = CoreRoot.LoadLastSave(Key: websvc.keys.lastview)
        {
            let track = Tracks.getTrack(id: lastView_id)
            self.performSegue(withIdentifier: "showDetail", sender: track)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
        
    }
    
    
    @objc func refreshAction(_ sender : UIRefreshControl)
    {
        if sender.isRefreshing { sender.endRefreshing() }
        searchiTunes()
    }
    
    // MARK: - Search
    func searchiTunes()
    {
        if SearchKeyString.count == 0 { return }
        let stringRequest = "term=\(SearchKeyString.replacingOccurrences(of: " ", with: "+"))\(appendKey)"
        let request = CoreRoot.shared.RegisterRequest(theService: stringRequest)
        CoreRoot.shared.LoadRequest(theRequest: request,  isJSON : true ) { (Data, hasResponse, ErrorCode) in
            if let DataDict = Data as? NSDictionary
            {
                if let ContentArray = DataDict["results"] as? [NSDictionary],(ErrorCode == 200) {
                    if ContentArray.count > 0
                    {
                        Tracks.saveAll(DataArray: ContentArray)
                        
                        let dateString = CoreRoot.currentStringDate()
                        self.configHeaderText(text: "LAST UPDATE: \(dateString)")
                        CoreRoot.StoreLastSave(data: dateString, byKey: websvc.keys.lastupdate)
                        CoreRoot.StoreLastSave(data: self.SearchKeyString, byKey: websvc.keys.lastkey)
                    }
                    else
                    {
                        CoreRoot.shared.showAlertView("Empty Search Result", message: "")
                        self.SearchKeyString = ""
                        CoreRoot.clearLastData(Key: websvc.keys.lastkey)
                    }
                    
                    
                }
                else if let errorMsg = DataDict["errorMessage"] as? String
                { CoreRoot.shared.showAlertView("Fetching Error", message: "\(errorMsg)") }
                else
                { CoreRoot.shared.showAlertView("Fetch Error", message: "Code: \(ErrorCode)") }
            }
            else
            { CoreRoot.shared.showAlertView("Fetch Error", message: "Code: \(ErrorCode)") }
            
            
            
        }
    }
    
    // MARK: - Nav Buttons
    @objc
    func SearchAction(_ sender: Any) {
        //  Tracks.deleteAll()
        
        let alertController = UIAlertController(title: "Search iTunes", message: "", preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = "enter keyword"
            
        }
        let confirmAction = UIAlertAction(title: "OK", style: .default) { [weak alertController] _ in
            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
            if let stringText = textField.text
            {
                self.SearchKeyString = stringText
                self.searchiTunes()
            }
            
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    @objc func InfoAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "ReadMe", message: "Use the \"SEARCH\" icon at the upperleft of the screen to search for iTunes Track. Select the \"Clear Search\" button to clear your search history." , preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Clear Search", style: .default) { _ in
            
            Tracks.deleteAll()
            self.SearchKeyString = ""
            CoreRoot.clearLastData(Key: websvc.keys.lastupdate)
            CoreRoot.clearLastData(Key: websvc.keys.lastkey)
            CoreRoot.clearLastData(Key: websvc.keys.lastview)
            self.configHeaderText(text: "Result : Empty Record", alignment: .right)
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            
            if let object = sender as? Tracks
            {
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
            
        }
    }
    
    
    
    // MARK: - Table View
    private func configHeaderText(text:String, alignment : NSTextAlignment = .center )
    {
        let view = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 1, height: TableHeight)))
        view.backgroundColor = .lightGray
        let label = UILabel()
        
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.textAlignment = alignment
        label.text = text
        view.addSubview(label)
        
        let trail = NSLayoutConstraint(item: label, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: -15)
        let  leading = NSLayoutConstraint(item: label, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 15)
        
        let height = NSLayoutConstraint(item: label, attribute: .height, relatedBy: .equal, toItem: view, attribute: .height, multiplier: 1, constant: 0)
        view.addConstraints([height, trail, leading])
        tableView.tableHeaderView = view
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier:  IconCell.id, for: indexPath) as? IconCell else { fatalError("Tablecell Crash") }
        let track = fetchedResultsController.object(at: indexPath)
        cell.configCell(track:track)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let track = fetchedResultsController.object(at: indexPath)
        self.performSegue(withIdentifier: "showDetail", sender: track)
        
    }
    
    
    // MARK: - Fetched results controller
    var fetchedResultsController: NSFetchedResultsController<Tracks> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<Tracks> = Tracks.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: "Master")
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController<Tracks>? = nil
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update: break
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    
}

